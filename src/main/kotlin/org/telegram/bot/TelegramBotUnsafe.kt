package org.telegram.bot

import com.google.gson.Gson
import com.google.gson.annotations.SerializedName
import com.google.gson.reflect.TypeToken

import okhttp3.OkHttpClient
import okhttp3.Request
import org.telegram.bot.inline.InlineQueryResult

import org.telegram.bot.message.*
import org.telegram.bot.message.reply.ReplyMarkup
import org.telegram.bot.wrapper.*
import org.telegram.bot.wrapper.chat.ChatWrapper

import java.net.URLEncoder

class TelegramBotUnsafe internal constructor(private val baseUrl: String) {
    internal val gson = Gson()
    internal val okhttp = OkHttpClient()

    private fun <T> request(typeOfT: TypeToken<Response<T>>, path: String, params: Map<String, Any?>? = null): T {
        val builder = StringBuilder()

        if (params != null) {
            var first = true
            for (p in params) {
                if (p.value != null) {
                    builder.append(if (first) { first = false; '?' } else '&')

                    builder.append("${p.key}=")
                    builder.append(
                        URLEncoder.encode(
                            if (p.value is List<*>) gson.toJson(p.value)
                            else p.value.toString()
                            , "utf-8"))
                }
            }
        }

        okhttp
            .newCall(Request.Builder().url(baseUrl + path + builder.toString()).build())
            .execute()
            .use {
                val response: Response<T> = gson.fromJson(it.body!!.string(), typeOfT.type)
                if (response.ok) return response.result!!
                else throw NokException(response.errorDescription!!, response.errorCode!!)
            }
    }

    fun getUpdates(offset: Int? = null, limit: Int? = null, timeout: Int? = null, allowedUpdates: List<String>? = null): List<UpdateWrapper> =
        request(object : TypeToken<Response<List<UpdateWrapper>>>() {}, "getUpdates", mapOf(
            "offset"          to offset,
            "limit"           to limit,
            "timeout"         to timeout,
            "allowed_updates" to allowedUpdates
        ))


    fun getMe(): UserWrapper = request(object : TypeToken<Response<UserWrapper>>() {}, "getMe")

    fun sendMessage(chatId: Long, text: String, parseMode: ParseMode? = null, entities: List<MessageEntity>? = null, disableWebPagePreview: Boolean? = null, disableNotification: Boolean? = null, replyToMessageId: Int? = null, allowSendingWithoutReply: Boolean? = null, replyMarkup: ReplyMarkup? = null): MessageWrapper {
        if (replyMarkup != null)
            TODO("Reply markup is not implemented")

        return request(object : TypeToken<Response<MessageWrapper>>() {}, "sendMessage",
            mapOf(
                "chat_id"                     to chatId,
                "text"                        to text,
                "parse_mode"                  to parseMode,
                "entities"                    to entities,
                "disable_web_page_preview"    to disableWebPagePreview,
                "disable_notification"        to disableNotification,
                "reply_to_message_id"         to replyToMessageId,
                "allow_sending_without_reply" to allowSendingWithoutReply,
                "reply_markup"                to replyMarkup
            )
        )
    }

    fun sendMessage(chatUsername: String, text: String, parseMode: ParseMode? = null, entities: List<MessageEntity>? = null, disableWebPagePreview: Boolean? = null, disableNotification: Boolean? = null, replyToMessageId: Int? = null, allowSendingWithoutReply: Boolean? = null, replyMarkup: ReplyMarkup? = null): MessageWrapper {
        if (replyMarkup != null)
            TODO("Reply markup is not implemented")

        return request(object : TypeToken<Response<MessageWrapper>>() {}, "sendMessage",
            mapOf(
                "chat_id"                     to "@$chatUsername",
                "text"                        to text,
                "parse_mode"                  to parseMode,
                "entities"                    to entities,
                "disable_web_page_preview"    to disableWebPagePreview,
                "disable_notification"        to disableNotification,
                "reply_to_message_id"         to replyToMessageId,
                "allow_sending_without_reply" to allowSendingWithoutReply,
                "reply_markup"                to replyMarkup
            )
        )
    }

    fun editMessageText(chatId: Long, messageId: Int, text: String, parseMode: ParseMode? = null, entities: List<MessageEntity>? = null, disableWebPagePreview: Boolean? = null, replyMarkup: ReplyMarkup? = null): MessageWrapper {
        if (replyMarkup != null)
            TODO("Reply markup is not implemented")

        return request(object : TypeToken<Response<MessageWrapper>>() {}, "editMessageText", mapOf(
            "chat_id"                  to chatId,
            "message_id"               to messageId,
            "text"                     to text,
            "parse_mode"               to parseMode,
            "entities"                 to entities,
            "disable_web_page_preview" to disableWebPagePreview,
            "reply_markup"             to replyMarkup
        ))
    }

    fun editMessageText(chatUsername: String, messageId: Int, text: String, parseMode: ParseMode? = null, entities: List<MessageEntity>? = null, disableWebPagePreview: Boolean? = null, replyMarkup: ReplyMarkup? = null): MessageWrapper {
        if (replyMarkup != null)
            TODO("Reply markup is not implemented")

        return request(object : TypeToken<Response<MessageWrapper>>() {}, "editMessageText", mapOf(
            "chat_id"                  to "@$chatUsername",
            "message_id"               to messageId,
            "text"                     to text,
            "parse_mode"               to parseMode,
            "entities"                 to entities,
            "disable_web_page_preview" to disableWebPagePreview,
            "reply_markup"             to replyMarkup
        ))
    }

    fun editInlineMessageText(inlineMessageId: Int, text: String, parseMode: ParseMode? = null, entities: List<MessageEntity>? = null, disableWebPagePreview: Boolean? = null, replyMarkup: ReplyMarkup? = null): Boolean {
        if (replyMarkup != null)
            TODO("Reply markup is not implemented")

        return request(object : TypeToken<Response<Boolean>>() {}, "editMessageText", mapOf(
            "inline_message_id"        to inlineMessageId,
            "text"                     to text,
            "parse_mode"               to parseMode,
            "entities"                 to entities,
            "disable_web_page_preview" to disableWebPagePreview,
            "reply_markup"             to replyMarkup
        ))
    }

    fun deleteMessage(chatId: Long, messageId: Int): Boolean {
        return request(object : TypeToken<Response<Boolean>>() {}, "deleteMessage", mapOf(
            "chat_id"    to chatId,
            "message_id" to messageId,
        ))
    }

    fun deleteMessage(chatUsername: String, messageId: Int): Boolean {
        return request(object : TypeToken<Response<Boolean>>() {}, "deleteMessage", mapOf(
            "chat_id"    to "@$chatUsername",
            "message_id" to messageId,
        ))
    }

    fun getChat(chatId: Long): ChatWrapper {
        return request(
            object : TypeToken<Response<ChatWrapper>>() {},
            "getChat",
            mapOf("chat_id" to chatId)
        )
    }

    fun getChat(chatUsername: String): ChatWrapper {
        return request(
            object : TypeToken<Response<ChatWrapper>>() {},
            "getChat",
            mapOf("chat_id" to "@$chatUsername")
        )
    }

    fun getFile(fileId: String): TelegramFileWrapper {
        return request(
            object : TypeToken<Response<TelegramFileWrapper>>() {},
            "getFile",
            mapOf("file_id" to fileId)
        )
    }

    fun answerInlineQuery(
        inlineQueryId: String,
        results: List<InlineQueryResult>,
        cacheTime: Int? = null,
        isPersonal: Boolean? = null,
        nextOffset: String? = null,
        switchPrivateChatText: String? = null,
        switchPrivateChatParameter: String? = null
    ): Boolean {
        return request(object : TypeToken<Response<Boolean>>() {}, "answerInlineQuery", mapOf(
            "inline_query_id"     to inlineQueryId,
            "results"             to results,
            "cache_time"          to cacheTime,
            "is_personal"         to isPersonal,
            "next_offset"         to nextOffset,
            "switch_pm_text"      to switchPrivateChatText,
            "switch_pm_parameter" to switchPrivateChatParameter
        ))
    }
}

private class Response<R>(val ok: Boolean) {
    val result: R? = null

    @SerializedName("error_code")
    val errorCode: Int? = null

    @SerializedName("description")
    val errorDescription: String? = null
}
