package org.telegram.bot.update

import org.telegram.bot.TelegramBot
import org.telegram.bot.message.Message
import org.telegram.bot.wrapper.UpdateWrapper

class MessageUpdate internal constructor(bot: TelegramBot, wrapper: UpdateWrapper) : Update(bot, wrapper) {
    /**
     * New incoming message of any kind — text, photo, sticker, etc
     */
    val message: Message = Message(bot, wrapper.message!!)
}
