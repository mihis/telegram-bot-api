package org.telegram.bot.update

import org.telegram.bot.TelegramBot
import org.telegram.bot.message.ChannelPost
import org.telegram.bot.message.Message
import org.telegram.bot.wrapper.UpdateWrapper

class EditedChannelPostUpdate internal constructor(bot: TelegramBot, wrapper: UpdateWrapper) : Update(bot, wrapper) {
    /**
     * New version of a channel post that is known to the bot and was edited
     */
    val channelPost: ChannelPost = ChannelPost(bot, wrapper.editedChannelPost!!)
}
