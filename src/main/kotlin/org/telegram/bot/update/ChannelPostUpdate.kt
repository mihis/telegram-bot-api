package org.telegram.bot.update

import org.telegram.bot.TelegramBot
import org.telegram.bot.message.ChannelPost
import org.telegram.bot.message.Message
import org.telegram.bot.wrapper.UpdateWrapper

class ChannelPostUpdate internal constructor(bot: TelegramBot, wrapper: UpdateWrapper) : Update(bot, wrapper) {
    /**
     * New incoming channel post of any kind — text, photo, sticker, etc
     */
    val channelPost: ChannelPost = ChannelPost(bot, wrapper.channelPost!!)
}
