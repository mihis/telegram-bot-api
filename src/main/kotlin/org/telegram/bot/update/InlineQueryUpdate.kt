package org.telegram.bot.update

import org.telegram.bot.TelegramBot
import org.telegram.bot.inline.InlineQuery
import org.telegram.bot.wrapper.UpdateWrapper

class InlineQueryUpdate internal constructor(bot: TelegramBot, wrapper: UpdateWrapper) : Update(bot, wrapper) {
    /**
     * New incoming inline query
     */
    val inlineQuery: InlineQuery = InlineQuery(bot, wrapper.inlineQuery!!)
}
