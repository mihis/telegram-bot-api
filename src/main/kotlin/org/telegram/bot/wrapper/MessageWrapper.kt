package org.telegram.bot.wrapper

import com.google.gson.annotations.SerializedName
import org.telegram.bot.data.Location

import org.telegram.bot.message.MessageEntity
import org.telegram.bot.wrapper.chat.ChatWrapper
import org.telegram.bot.wrapper.voice.*

data class MessageWrapper(
    @SerializedName("message_id")
    val id: Int,

    val from: UserWrapper?,

    @SerializedName("sender_chat")
    val senderChat: ChatWrapper?,

    val date: Long,

    val chat: ChatWrapper,

    @SerializedName("forward_from")
    val forwardFrom: UserWrapper?,

    @SerializedName("forward_from_chat")
    val forwardFromChat: ChatWrapper?,

    @SerializedName("forward_from_message_id")
    val forwardFromMessageId: Int?,

    @SerializedName("forward_signature")
    val forwardSignature: String?,

    @SerializedName("forward_sender_name")
    val forwardSenderName: String?,

    @SerializedName("forward_date")
    val forwardDate: Int?,

    @SerializedName("reply_to_message")
    val replyToMessage: MessageWrapper?,

    @SerializedName("via_bot")
    val viaBot: UserWrapper?,

    @SerializedName("edit_date")
    val editDate: Long?,

    @SerializedName("media_group_id")
    val mediaGroupId: String?,

    @SerializedName("author_signature")
    val authorSignature: String?,

    val text: String?,

    val entities: List<MessageEntity>?,

    val animation: List<AnimationWrapper>?,

    val audio: AudioWrapper?,

    val document: DocumentWrapper?,

    val photo: List<PhotoSizeWrapper>?,

    val sticker: StickerWrapper?,

    val video: VideoWrapper?,

    @SerializedName("video_note")
    val video_note: VideoNoteWrapper?,

    val voice: VoiceWrapper?,

    val caption: String?,

    @SerializedName("caption_entities")
    val caption_entities: List<MessageEntity>?,

    val contact: ContactWrapper?,

    val dice: DiceWrapper?,

    val game: GameWrapper?,

    val poll: PollWrapper?,

    val venue: VenueWrapper?,

    val location: Location?,

    @SerializedName("new_chat_members")
    val newChatMembers: List<UserWrapper>?,

    @SerializedName("left_chat_member")
    val leftChatMember: UserWrapper?,

    @SerializedName("new_chat_title")
    val newChatTitle: String?,

    @SerializedName("new_chat_photo")
    val newChatPhoto: List<PhotoSizeWrapper>?,

    @SerializedName("delete_chat_photo")
    val delete_chat_photo: Boolean?,

    @SerializedName("group_chat_created")
    val groupChatCreated: Boolean?,

    @SerializedName("supergroup_chat_created")
    val supergroupChatCreated: Boolean?,

    @SerializedName("channel_chat_created")
    val channelChatCreated: Boolean?,

    @SerializedName("message_auto_delete_timer_changed")
    val messageAutoDeleteTimerChanged: MessageAutoDeleteTimerChangedWrapper?,

    @SerializedName("migrate_to_chat_id")
    val migrateToChatId: Int?,

    @SerializedName("migrate_from_chat_id")
    val migrateFromChatId: Int?,

    @SerializedName("pinned_message")
    val pinnedMessage: MessageWrapper?,

    val invoice: InvoiceWrapper?,

    @SerializedName("successful_payment")
    val successfulPayment: SuccessfulPaymentWrapper?,

    @SerializedName("connected_website")
    val connectedWebsite: String?,

    @SerializedName("passport_data")
    val passportData: PassportDataWrapper?,

    @SerializedName("proximity_alert_triggered")
    val proximityAlertTriggered: ProximityAlertTriggeredWrapper?,

    @SerializedName("voice_chat_scheduled")
    val voiceChatScheduled: VoiceChatScheduledWrapper?,

    @SerializedName("voice_chat_started")
    val voiceChatStarted: VoiceChatStartedWrapper?,

    @SerializedName("voice_chat_ended")
    val voiceChatEnded: VoiceChatEndedWrapper?,

    @SerializedName("voice_chat_participants_invited")
    val voiceChatParticipantsInvited: VoiceChatParticipantsInvitedWrapper?,

    @SerializedName("reply_markup")
    val replyMarkup: InlineKeyboardMarkupWrapper?)
