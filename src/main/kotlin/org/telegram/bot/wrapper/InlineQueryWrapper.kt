package org.telegram.bot.wrapper

import com.google.gson.annotations.SerializedName
import org.telegram.bot.data.Location
import org.telegram.bot.wrapper.chat.ChatTypeWrapper

data class InlineQueryWrapper(
    val id: String,
    val from: UserWrapper,

    // Text of the query (up to 256 characters)
    val query: String,

    // Offset of the results to be returned, can be controlled by the bot
    val offset: String,

    // Optional. Type of the chat, from which the inline query was sent. Can be either “sender” for a private chat with the inline query sender, “private”, “group”, “supergroup”, or “channel”. The chat type should be always known for requests sent from official clients and most third-party clients, unless the request was sent from a secret chat
    @SerializedName("chat_type")
    val chatType: ChatTypeWrapper?,

    // Optional. Sender location, only for bots that request user location
    val location: Location?
)
