package org.telegram.bot.wrapper

import com.google.gson.annotations.SerializedName

data class UpdateWrapper(
    @SerializedName("update_id")
    val id: Int,

    val message: MessageWrapper?,

    @SerializedName("edited_message")
    val editedMessage: MessageWrapper?,

    @SerializedName("channel_post")
    val channelPost: MessageWrapper?,

    @SerializedName("edited_channel_post")
    val editedChannelPost: MessageWrapper?,

    @SerializedName("inline_query")
    val inlineQuery: InlineQueryWrapper?,

    @SerializedName("chosen_inline_result")
    val chosenInlineResult: ChosenInlineResultWrapper?,

    @SerializedName("callback_query")
    val callbackQuery: CallbackQueryWrapper?,

    @SerializedName("shipping_query")
    val shippingQuery: ShippingQueryWrapper?,

    @SerializedName("pre_checkout_query")
    val preCheckoutQuery: PreCheckoutQueryWrapper?,

    val poll: PollWrapper?,

    @SerializedName("poll_answer")
    val pollAnswer: PollAnswerWrapper?,

    @SerializedName("my_chat_member")
    val myChatMember: ChatMemberUpdatedWrapper?,

    @SerializedName("chat_member")
    val chatMember: ChatMemberUpdatedWrapper?)
