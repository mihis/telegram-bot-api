package org.telegram.bot.wrapper.chat

import com.google.gson.annotations.SerializedName

import org.telegram.bot.chat.ChatLocation
import org.telegram.bot.chat.ChatPermissions
import org.telegram.bot.wrapper.MessageWrapper

data class ChatWrapper(
    val id: Long,

    val type: ChatTypeWrapper,

    val title: String?,

    val username: String?,

    @SerializedName("first_name")
    val firstname: String?,

    @SerializedName("last_name")
    val lastname: String?,

    val photo: ChatPhotoWrapper?,

    val bio: String?,

    val description: String?,

    @SerializedName("invite_link")
    val inviteLink: String?,

    @SerializedName("pinned_message")
    val pinnedMessage: MessageWrapper?,

    val permissions: ChatPermissions?,

    @SerializedName("slow_mode_delay")
    val slowModeDelay: Int?,

    @SerializedName("message_auto_delete_time")
    val messageAutoDeleteTime: Int?,

    @SerializedName("sticker_set_name")
    val stickerSetName: String?,

    @SerializedName("can_set_sticker_set")
    val canSetStickerSet: Boolean?,

    @SerializedName("linked_chat_id")
    val linkedChatId: Int?,

    val location: ChatLocation?)
