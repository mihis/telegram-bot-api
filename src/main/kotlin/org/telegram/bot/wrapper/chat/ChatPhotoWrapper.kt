package org.telegram.bot.wrapper.chat

import com.google.gson.annotations.SerializedName

data class ChatPhotoWrapper(
    @SerializedName("small_file_id")
    val smallFileId: String,

    @SerializedName("small_file_unique_id")
    val smallFileUniqueId: String,

    @SerializedName("big_file_id")
    val bigFileId: String,

    @SerializedName("big_file_unique_id")
    val bigFileUniqueId: String)
