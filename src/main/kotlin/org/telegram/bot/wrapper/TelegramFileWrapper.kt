package org.telegram.bot.wrapper

import com.google.gson.annotations.SerializedName

data class TelegramFileWrapper(
    @SerializedName("file_id")
    val id: String,

    @SerializedName("file_unique_id")
    val uniqueId: String,

    @SerializedName("file_size")
    val size: Int?,

    @SerializedName("file_path")
    val path: String?
)
