package org.telegram.bot.user

import org.telegram.bot.TelegramBot
import org.telegram.bot.wrapper.UserWrapper

/**
 * Represents the you in [TelegramBot.getMe]
 */
class MeUser internal constructor(bot: TelegramBot, wrapper: UserWrapper) : User(bot, wrapper) {
    /**
     * True, if the bot can be invited to groups
     */
    val canJoinGroups: Boolean = wrapper.canJoinGroups!!

    /**
     * True, if privacy mode is disabled for the bot
     */
    val canReadAllGroupMessages: Boolean = wrapper.canReadAllGroupMessages!!

    /**
     * True, if the bot supports inline queries
     */
    val supportsInlineQueries: Boolean = wrapper.supportsInlineQueries!!
}
