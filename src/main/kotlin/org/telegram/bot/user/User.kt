package org.telegram.bot.user

import org.telegram.bot.TelegramBot
import org.telegram.bot.chat.PrivateChat
import org.telegram.bot.wrapper.UserWrapper

/**
 * Represents a Telegram user or bot
 */
open class User internal constructor(val bot: TelegramBot, internal val wrapper: UserWrapper) {
    /**
     * Unique identifier for this user or bot.
     */
    val id: Long = wrapper.id

    /**
     * True, if this user is a bot
     */
    val isBot: Boolean = wrapper.isBot

    /**
     * User's or bot's first name
     */
    val firstname: String = wrapper.firstname

    /**
     * User's or bot's last name
     */
    val lastname: String? = wrapper.lastname

    /**
     * User's or bot's username
     */
    val username: String? = wrapper.username

    /**
     * IETF language tag of the user's language
     */
    val languageCode: String? = wrapper.languageCode

    fun getPrivateChat(): PrivateChat = bot.getPrivateChat(id)!!

    override fun equals(other: Any?): Boolean = if (other is User) this.id == other.id else false

    override fun hashCode(): Int = id.hashCode()
}
