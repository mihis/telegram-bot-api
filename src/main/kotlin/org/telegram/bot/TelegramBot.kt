package org.telegram.bot

import org.telegram.bot.chat.*
import org.telegram.bot.chat.detailed.*
import org.telegram.bot.message.*
import org.telegram.bot.message.reply.ReplyMarkup
import org.telegram.bot.update.*
import org.telegram.bot.user.*
import org.telegram.bot.wrapper.chat.ChatTypeWrapper

class TelegramBot(val token: String) {
    val unsafe = TelegramBotUnsafe("https://api.telegram.org/bot$token/")

    @JvmOverloads
    fun getUpdates(offset: Int? = null,
                   limit: Int? = null,
                   timeout: Int? = null,
                   allowedUpdates: List<String>? = null): List<Update> =
        unsafe.getUpdates(offset, limit, timeout, allowedUpdates).map {
            when { // ToDo: Add more supported message types
                it.message != null -> MessageUpdate(this, it)
                it.editedMessage != null -> EditedMessageUpdate(this, it)
                else -> Update(this, it)
            }
        }

    fun getMe(): MeUser = MeUser(this, unsafe.getMe())

    @JvmOverloads
    fun editInlineMessageText(inlineMessageId: Int,
                              text: String,
                              parseMode: ParseMode? = null,
                              entities: List<MessageEntity>? = null,
                              disableWebPagePreview: Boolean? = null,
                              replyMarkup: ReplyMarkup? = null): Boolean =
        unsafe.editInlineMessageText(
            inlineMessageId,
            text,
            parseMode,
            entities,
            disableWebPagePreview,
            replyMarkup
        )

    fun getChat(id: Long): Chat {
        val chat = unsafe.getChat(id)
        return when (chat.type) {
            ChatTypeWrapper.PRIVATE -> DetailedPrivateChat(this, chat)
            ChatTypeWrapper.GROUP -> DetailedGroupChat(this, chat)
            ChatTypeWrapper.SUPERGROUP -> DetailedSupergroupChat(this, chat)
            ChatTypeWrapper.CHANNEL -> DetailedChannelChat(this, chat)
        }
    }

    fun getChat(username: String): Chat {
        val chat = unsafe.getChat(username)
        return when (chat.type) {
            ChatTypeWrapper.PRIVATE -> DetailedPrivateChat(this, chat)
            ChatTypeWrapper.GROUP -> DetailedGroupChat(this, chat)
            ChatTypeWrapper.SUPERGROUP -> DetailedSupergroupChat(this, chat)
            ChatTypeWrapper.CHANNEL -> DetailedChannelChat(this, chat)
        }
    }

    fun getPrivateChat(id: Long): DetailedPrivateChat? {
        val chat = getChat(id)
        return if (chat is DetailedPrivateChat) chat else null
    }

    fun getGroupChat(id: Long): DetailedGroupChat? {
        val chat = getChat(id)
        return if (chat is DetailedGroupChat) chat else null
    }

    fun getSupergroupChat(id: Long): DetailedSupergroupChat? {
        val chat = getChat(id)
        return if (chat is DetailedSupergroupChat) chat else null
    }

    fun getChannelChat(id: Long): DetailedChannelChat? {
        val chat = getChat(id)
        return if (chat is DetailedChannelChat) chat else null
    }
}
