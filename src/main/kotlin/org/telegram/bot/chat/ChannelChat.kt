package org.telegram.bot.chat

import org.telegram.bot.TelegramBot
import org.telegram.bot.chat.detailed.DetailedChannelChat
import org.telegram.bot.wrapper.chat.ChatWrapper

/**
 * Represents a channel
 */
open class ChannelChat(bot: TelegramBot, wrapper: ChatWrapper) : Chat(bot, wrapper) {
    /**
     * Title of chat
     */
    val title: String = wrapper.title!!

    /**
     * Username of chat
     */
    val username: String? = wrapper.username

    open fun detailed(): DetailedChannelChat = bot.getChannelChat(id)!!
}