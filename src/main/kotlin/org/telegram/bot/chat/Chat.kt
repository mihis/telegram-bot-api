package org.telegram.bot.chat

import org.telegram.bot.TelegramBot
import org.telegram.bot.message.AbstractMessage
import org.telegram.bot.message.MessageEntity
import org.telegram.bot.message.ParseMode
import org.telegram.bot.message.reply.ReplyMarkup
import org.telegram.bot.wrapper.chat.ChatWrapper

/**
 * Represents a chat
 */
open class Chat internal constructor(val bot: TelegramBot, internal val wrapper: ChatWrapper) {
    /**
     * Unique identifier for this chat
     */
    val id: Long = wrapper.id

    /**
     * Sends text message
     *
     * @param text text of the message to be sent, 1-4096 characters after entities parsing
     * @param parseMode mode for parsing entities in the message text. See formatting options for more details
     * @param entities list of special entities that appear in message text, which can be specified instead of parse_mode
     * @param disableWebPagePreview disables link previews for links in this message
     * @param disableNotification sends the message silently. Users will receive a notification with no sound
     * @param replyToMessageId if the message is a reply, ID of the original message
     * @param allowSendingWithoutReply pass True, if the message should be sent even if the specified replied-to message is not found
     * @param replyMarkup Additional interface options. Object for an inline keyboard, custom reply keyboard, instructions to remove reply keyboard or to force a reply from the user
     */
    @JvmOverloads
    fun sendMessage(text: String,
                    parseMode: ParseMode? = null,
                    entities: List<MessageEntity>? = null,
                    disableWebPagePreview: Boolean? = null,
                    disableNotification: Boolean? = null,
                    replyToMessageId: Int? = null,
                    allowSendingWithoutReply: Boolean? = null,
                    replyMarkup: ReplyMarkup? = null) : AbstractMessage =
        AbstractMessage(bot, bot.unsafe.sendMessage(id, text, parseMode, entities, disableWebPagePreview, disableNotification, replyToMessageId, allowSendingWithoutReply, replyMarkup))

    /**
     * Use this method to delete a message, including service messages, with the following limitations:
     * - A message can only be deleted if it was sent less than 48 hours ago.
     * - A dice message in a private chat can only be deleted if it was sent more than 24 hours ago.
     * - Bots can delete outgoing messages in private chats, groups, and supergroups.
     * - Bots can delete incoming messages in private chats.
     * - Bots granted can_post_messages permissions can delete outgoing messages in channels.
     * - If the bot is an administrator of a group, it can delete any message there.
     * - If the bot has can_delete_messages permission in a supergroup or a channel, it can delete any message there.
     *
     * @param id id of message to delete
     * @return true on success.
     */
    fun deleteMessage(id: Int): Boolean = bot.unsafe.deleteMessage(this.id, id)
}
