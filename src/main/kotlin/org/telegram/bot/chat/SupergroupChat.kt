package org.telegram.bot.chat

import org.telegram.bot.TelegramBot
import org.telegram.bot.chat.detailed.DetailedPrivateChat
import org.telegram.bot.chat.detailed.DetailedSupergroupChat
import org.telegram.bot.wrapper.chat.ChatWrapper

/**
 * Represents a supergroup
 */
open class SupergroupChat internal constructor(bot: TelegramBot, wrapper: ChatWrapper) : Chat(bot, wrapper) {
    /**
     * Title of chat
     */
    val title: String = wrapper.title!!

    /**
     * Username of chat
     */
    val username: String? = wrapper.username

    open fun detailed(): DetailedSupergroupChat = bot.getSupergroupChat(id)!!
}
