package org.telegram.bot.chat

import org.telegram.bot.data.Location

data class ChatLocation(val location: Location, val address: String)
