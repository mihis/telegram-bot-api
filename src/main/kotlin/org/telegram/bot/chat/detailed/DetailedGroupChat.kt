package org.telegram.bot.chat.detailed

import org.telegram.bot.TelegramBot
import org.telegram.bot.chat.ChatPermissions
import org.telegram.bot.chat.ChatPhoto
import org.telegram.bot.chat.GroupChat
import org.telegram.bot.message.AbstractMessage
import org.telegram.bot.wrapper.chat.ChatWrapper

class DetailedGroupChat internal constructor(bot: TelegramBot, wrapper: ChatWrapper) : GroupChat(bot, wrapper) {
    /**
     * Chat photo
     */
    val photo: ChatPhoto? = if (wrapper.photo == null) null else ChatPhoto(bot, wrapper.photo)

    /**
     * Description of chat
     */
    val description: String? = wrapper.description

    /**
     * Primary invite link
     */
    val inviteLink: String? = wrapper.inviteLink

    /**
     * The most recent pinned message (by sending date)
     */
    val pinnedMessage: AbstractMessage? = if (wrapper.pinnedMessage == null) null else AbstractMessage(bot, wrapper.pinnedMessage)

    /**
     * Default chat member permissions
     */
    val permissions: ChatPermissions = wrapper.permissions!!

    /**
     * The time after which all messages sent to the chat will be automatically deleted; in seconds
     */
    val messageAutoDeleteTime: Int = wrapper.messageAutoDeleteTime!!

    override fun detailed(): DetailedGroupChat = this
}
