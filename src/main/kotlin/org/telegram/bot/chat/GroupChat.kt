package org.telegram.bot.chat

import org.telegram.bot.TelegramBot
import org.telegram.bot.chat.detailed.DetailedChannelChat
import org.telegram.bot.chat.detailed.DetailedGroupChat
import org.telegram.bot.wrapper.chat.ChatWrapper

/**
 * Represents a group chat
 */
open class GroupChat internal constructor(bot: TelegramBot, wrapper: ChatWrapper) : Chat(bot, wrapper) {
    /**
     * Title of chat
     */
    val title: String = wrapper.title!!

    open fun detailed(): DetailedGroupChat = bot.getGroupChat(id)!!
}