package org.telegram.bot.chat

import org.telegram.bot.TelegramBot
import org.telegram.bot.data.TelegramFile
import org.telegram.bot.wrapper.chat.ChatPhotoWrapper

/**
 * Represents a chat photo
 */
class ChatPhoto internal constructor(val bot: TelegramBot, val wrapper: ChatPhotoWrapper) {
    /**
     * File of small (160x160) chat photo
     */
    val smallFile: TelegramFile = TelegramFile(bot, bot.unsafe.getFile(wrapper.smallFileId))

    /**
     * File of big (640x640) chat photo
     */
    val bigFile: TelegramFile = TelegramFile(bot, bot.unsafe.getFile(wrapper.smallFileId))
}
