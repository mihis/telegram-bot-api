package org.telegram.bot.data

import okhttp3.Request

import org.telegram.bot.TelegramBot
import org.telegram.bot.wrapper.TelegramFileWrapper

import java.io.InputStream

class TelegramFile internal constructor(val bot: TelegramBot, val wrapper: TelegramFileWrapper) {
    /**
     * Identifier for this file
     */
    val id: String = wrapper.id

    /**
     * Unique identifier for this file,
     * which is supposed to be the same
     * over time and for different bots.
     *
     * Can't be used to download or reuse the file.
     */
    val uniqueId: String = wrapper.uniqueId

    /**
     * File size, if known
     */
    val size: Int? = wrapper.size

    fun download(): InputStream =
        bot.unsafe
            .okhttp
            .newCall(Request.Builder().url("https://api.telegram.org/file/bot${bot.token}/${wrapper.path}").build())
            .execute()
            .body!!
            .byteStream()
}