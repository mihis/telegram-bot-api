package org.telegram.bot.message

import org.telegram.bot.TelegramBot
import org.telegram.bot.chat.Chat
import org.telegram.bot.message.reply.ReplyMarkup
import org.telegram.bot.wrapper.MessageWrapper

/**
 * Represents a message
 */
open class AbstractMessage internal constructor(val bot: TelegramBot, internal var wrapper: MessageWrapper) {
    /**
     * Unique message identifier inside this chat
     */
    val id: Int
        get() = wrapper.id

    /**
     * Date the message was sent in Unix time
     */
    val date: Long
        get() = wrapper.date

    /**
     * Conversation the message belongs to
     */
    open val chat: Chat
        get() = Chat(bot, wrapper.chat)

    /**
     * Date the message was last edited in Unix time
     */
    val editDate: Long?
        get() = wrapper.editDate

    /**
     * For text messages, the actual UTF-8 text of the message, 0-4096 characters
     */
    val text: String
        get() = wrapper.text ?: ""

    /**
     * For text messages, special entities like usernames, URLs, bot commands, etc. that appear in the text
     */
    val entities: List<MessageEntity>
        get() = wrapper.entities ?: listOf()

    /**
     * Use this method to edit text and game messages
     */
    @JvmOverloads
    fun editText(text: String,
                 parseMode: ParseMode? = null,
                 entities: List<MessageEntity>? = null,
                 disableWebPagePreview: Boolean? = null,
                 replyMarkup: ReplyMarkup? = null) {
        wrapper = bot.unsafe.editMessageText(chat.id, id, text, parseMode, entities, disableWebPagePreview, replyMarkup)
    }

    /**
     * Use this method to delete a message, including service messages, with the following limitations:
     * - A message can only be deleted if it was sent less than 48 hours ago.
     * - A dice message in a private chat can only be deleted if it was sent more than 24 hours ago.
     * - Bots can delete outgoing messages in private chats, groups, and supergroups.
     * - Bots can delete incoming messages in private chats.
     * - Bots granted can_post_messages permissions can delete outgoing messages in channels.
     * - If the bot is an administrator of a group, it can delete any message there.
     * - If the bot has can_delete_messages permission in a supergroup or a channel, it can delete any message there.
     *
     * @return true on success.
     */
    fun delete(): Boolean = chat.deleteMessage(id)

    /**
     * Sends text message in reply to this message
     *
     * @param text text of the message to be sent, 1-4096 characters after entities parsing
     * @param parseMode mode for parsing entities in the message text. See formatting options for more details
     * @param entities list of special entities that appear in message text, which can be specified instead of parse_mode
     * @param disableWebPagePreview disables link previews for links in this message
     * @param disableNotification sends the message silently. Users will receive a notification with no sound
     * @param allowSendingWithoutReply pass True, if the message should be sent even if the specified replied-to message is not found
     * @param replyMarkup Additional interface options. Object for an inline keyboard, custom reply keyboard, instructions to remove reply keyboard or to force a reply from the user
     */
    @JvmOverloads
    fun reply(text: String,
              parseMode: ParseMode? = null,
              entities: List<MessageEntity>? = null,
              disableWebPagePreview: Boolean? = null,
              disableNotification: Boolean? = null,
              allowSendingWithoutReply: Boolean? = null,
              replyMarkup: ReplyMarkup? = null): AbstractMessage =
        chat.sendMessage(text, parseMode, entities, disableWebPagePreview, disableNotification, id, allowSendingWithoutReply, replyMarkup)
}
