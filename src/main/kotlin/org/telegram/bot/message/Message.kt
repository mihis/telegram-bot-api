package org.telegram.bot.message

import org.telegram.bot.TelegramBot
import org.telegram.bot.user.User
import org.telegram.bot.wrapper.MessageWrapper

class Message(bot: TelegramBot, wrapper: MessageWrapper) : AbstractMessage(bot, wrapper) {
    val from: User
        get() = User(bot, wrapper.from!!)
}
