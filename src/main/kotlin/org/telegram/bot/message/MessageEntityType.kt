package org.telegram.bot.message

import com.google.gson.annotations.SerializedName

enum class MessageEntityType {
    @SerializedName("mention")
    MENTION,

    @SerializedName("hashtag")
    HASHTAG,

    @SerializedName("cashtag")
    CASHTAG,

    @SerializedName("bot_command")
    BOT_COMMAND,

    @SerializedName("url")
    URL,

    @SerializedName("email")
    EMAIL,

    @SerializedName("phone_number")
    PHONE_NUMBER,

    @SerializedName("bold")
    BOLD,

    @SerializedName("italic")
    ITALIC,

    @SerializedName("underline")
    UNDERLINE,

    @SerializedName("strikethrough")
    STRIKETHROUGH,

    @SerializedName("code")
    CODE,

    @SerializedName("pre")
    PRE,

    @SerializedName("text_link")
    TEXT_LINK,

    @SerializedName("text_mention")
    TEXT_MENTION;
}
