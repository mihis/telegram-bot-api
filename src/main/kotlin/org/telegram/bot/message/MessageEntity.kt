package org.telegram.bot.message

import org.telegram.bot.wrapper.UserWrapper

data class MessageEntity(
    val type: MessageEntityType,
    val offset: Int,
    val length: Int,
    val url: String?,
    val user: UserWrapper?,
    val language: String?)
