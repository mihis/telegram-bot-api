package org.telegram.bot.message

import org.telegram.bot.TelegramBot
import org.telegram.bot.chat.ChannelChat
import org.telegram.bot.wrapper.MessageWrapper

class ChannelPost(bot: TelegramBot, wrapper: MessageWrapper) : AbstractMessage(bot, wrapper) {
    override val chat: ChannelChat
        get() = ChannelChat(bot, wrapper.chat)
}
